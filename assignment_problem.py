from ford_fulkerson import FordFulkersonSolver


class AssignmentProblemSolver(object):
    def __init__(self, c):
        self.c = c
        self.init_c = c[:]
        self.n = len(c)
        self.x = [[0] * len(c) for i in range(len(c))]
        self.B = []
        self.F = 1e9 + 7

    def solve(self):
        n = self.n
        s = 0
        t = 2 * n + 1
        INF = 1e9 + 7

        while True:
            self.c = [[item - min(row) for item in row] for row in self.c]
            mins = [min([row[ind] for row in self.c]) for ind in range(n)]
            self.c = [[row[j] - mins[j] for j in range(n)] for row in self.c]

            U0 = [[i + 1, j + n + 1] for i in range(n) for j in range(n) if self.c[i][j] == 0]
            U1 = [[s, i + 1] for i in range(n)]
            Uast = [[n + i + 1, t] for i in range(n)]

            IS = [[i] for i in range(t + 1)]
            IS = [row[:len(row) - 1] for row in IS]

            for edge in U1:
                IS[edge[0]].append([edge[1], 1])

            for edge in Uast:
                IS[edge[0]].append([edge[1], 1])

            for edge in U0:
                IS[edge[0]].append([edge[1], INF])

            solver = FordFulkersonSolver(IS)
            res = solver.max_flow()

            if res.ans == n:
                self.F = 0
                for ind in range(1, n + 1):
                    for edge in res.f[ind]:
                        i = ind - 1
                        j = edge[0] - 1 - n
                        if edge[1] == 1:
                            self.B.append([i, j])
                            self.x[i][j] = 1
                            self.F += self.x[i][j] * self.init_c[i][j]

                return self

            N1 = [ind for ind in res.U if 0 < ind <= n]
            N2 = [ind - n for ind in res.U if ind not in N1 and ind > 0]

            alpha = min([self.c[i - 1][j - 1] for i in N1 for j in range(1, n + 1) if j not in N2])

            for i in range(1, n + 1):
                for j in range(1, n + 1):
                    if i in N1 and j not in N2:
                        self.c[i - 1][j - 1] -= alpha

                    elif i not in N1 and j in N2:
                        self.c[i - 1][j - 1] += alpha

        pass


if __name__ == '__main__':
    c = [
        [2, 10, 9, 7],
        [15, 4, 14, 8],
        [13, 14, 16, 11],
        [4, 15, 13, 19]
    ]

    solver = AssignmentProblemSolver(c)
    res = solver.solve()

    print(res.B)