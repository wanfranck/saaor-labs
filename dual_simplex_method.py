import numpy as np
from functools import reduce
from simplex_method import SimplexMethodSolver

class DualSimplexMethodSolver(object):
    def __init__(self, A, b, c, dl, dh, Jb=None):
        self.A = A
        self.m, self.n = A.shape
        self.b = np.array([b]).T
        self.c = np.array([c])
        self.dl = np.array([dl])
        self.dh = np.array([dh])
        self.Jb = Jb
        self.hi = None
        self.max = None

    def solve(self):
        eps = 1e-9

        # form A-base and inv-A-base, also c-base
        Ab = self.A[:, self.Jb]
        B = np.linalg.inv(Ab)
        cb = self.c[:, self.Jb]

        # count y as cb * B
        y = np.dot(cb, B)

        # count delta as y * A - c foreach j in J
        delta = np.dot(y, self.A) - self.c

        J = [ind for ind in range(0, self.n)]

        # make Jn, Jn_pos and Jn_neg
        Jn = [ind for ind in J if ind not in self.Jb]
        Jn_pos = [ind for ind in Jn if delta[0][ind] >= eps]
        Jn_neg = [ind for ind in Jn if ind not in Jn_pos]

        STEPS = 10000
        while STEPS:
            STEPS -= 1

            def hi_func_non_base(ind):
                if ind in Jn_pos:
                    return self.dl[0][ind]
                elif ind in Jn_neg:
                    return self.dh[0][ind]
                else:
                    return None

            hi = np.array([hi_func_non_base(ind) for ind in J])
            prods = [np.dot(np.array([self.A[:, ind]]).T, hi[ind]) for ind in Jn]
            hib = np.dot(B, self.b - sum(prods)).T

            for ind, val in enumerate(self.Jb):
                hi[val] = hib[0][ind]

            is_optimal = all([self.dh[0][ind] + eps >= hi[ind] >= self.dl[0][ind] - eps for ind in self.Jb])

            if is_optimal:
                self.hi, self.max, self.steps = hi, np.dot(self.c, hi)[0], 10000 - STEPS
                return self

            jk, k = np.inf, -1
            for ind, val in enumerate(self.Jb):
                if self.dh[0][val] < hi[val] + eps or hi[val] < self.dl[0][val] - eps:
                    if val < jk:
                        jk, k = val, ind

            e = np.zeros(len(self.Jb))
            e[k] = 1.0
            mu_jk = 1.0 if hi[jk] < self.dl[0][jk] else -1.0
            delta_y = np.dot(mu_jk, np.dot(np.eye(self.m, self.m)[:,k], B))
            mu = [np.dot(delta_y, self.A[:, ind]) for ind in Jn]

            def ok(ind, val):
                return (val in Jn_pos and mu[ind] < 0.0) or (val in Jn_neg and mu[ind] > 0.0)
            sigma = [[val, -delta[0][val] / mu[ind] if ok(ind, val) else np.inf] for ind, val in enumerate(Jn)]

            def min_sigma(x, y):
                if x[1] > y[1]:
                    return y
                else:
                    return x

            sigma0 = reduce(min_sigma, sigma, [-1, np.inf])
            if sigma0[1] == np.inf:
                self.steps = 10000 - STEPS
                return self

            def delta_func(ind):
                if ind == jk:
                    return delta[0][ind] + sigma0[1] * mu_jk
                elif ind in Jn:
                    return delta[0][ind] + sigma0[1] * mu[Jn.index(ind)]
                elif ind in self.Jb:
                    return 0.0

            new_coplan = [delta_func(ind) for ind in J]
            delta[0] = new_coplan

            self.Jb[k] = sigma0[0]
            Ab = self.A[:, self.Jb]
            B = np.linalg.inv(Ab)

            Jn = [ind for ind in J if ind not in self.Jb]
            if mu_jk == 1.0:
                if sigma0[0] in Jn_pos:
                    Jn_pos[Jn_pos.index(sigma0[0])] = jk
                else:
                    Jn_pos.append(jk)
            else:
                if sigma0[0] in Jn_pos:
                    Jn_pos.pop(Jn_pos.index(sigma0[0]))

            Jn_neg = [ind for ind in Jn if ind not in Jn_pos]
            self.B, self.Ab = B, Ab

        self.hi, self.max, self.steps = 'Too far', 'Too lazy to wait', 10000
        return self

if __name__ == '__main__':
    A = np.array([
        [2, 1, -1, 0, 0, 1],
        [1, 0, 1, 1, 0, 0],
        [0, 1, 0, 0, 1, 0]
    ])

    b = np.array([2, 5, 0])
    c = np.array([3, 2, 0, 3, -2, -4])
    dl = np.array([0, -1, 2, 1, -1, 0])
    dh = np.array([2, 4, 4, 3, 3, 5])
    Jb = SimplexMethodSolver(A, b, c).first_phase().basic_indexes

    solver = DualSimplexMethodSolver(A, b, c, dl, dh, Jb)
    res = solver.solve()

    print(res.max)

    A = np.array([
        [0, 7, 1, -1, -4, 2, 4],
        [5, 1, 4, 3, -5, 2, 1],
        [2, 0, 3, 1, 0, 1, 5]
    ])

    b = np.array([12, 27, 19])
    c = np.array([10, 2, 1, 7, 6, 3, 1])
    dl = np.array([0, 0, 0, 0, 0, 0, 0])
    inf = 1e32
    dh = np.array([inf, inf, inf, inf, inf, inf, inf])
    Jb = SimplexMethodSolver(A, b, c).first_phase().basic_indexes

    solver = DualSimplexMethodSolver(A, b, c, dl, dh, Jb)
    res = solver.solve()

    print(res.max)

    A = np.array([
        [1, -5, 3, 1, 0, 0],
        [4, -1, 1, 0, 1, 0],
        [2, 4, 2, 0, 0, 1]
    ])

    b = np.array([-8, 28, 30])
    c = np.array([7, -2, 6, 0, 5, 2])
    dl = np.array([2, 1, 0, 0, 1, 1])
    dh = np.array([6, 6, 5, 2, 4, 6])
    Jb = SimplexMethodSolver(A, b, c).first_phase().basic_indexes

    solver = DualSimplexMethodSolver(A, b, c, dl, dh, Jb)
    res = solver.solve()

    print(res.max)

    A = np.array([
        [1, -3, 2, 0, 1, -1, 4, -1, 0],
        [1, -1, 6, 1, 0, -2, 2, 2, 0],
        [2, 2, -1, 1, 0, -3, 8, -1, 1],
        [4, 1, 0, 0, 1, -1, 0, -1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1, 1]
    ])

    b = np.array([3, 9, 9, 5, 9])
    c = np.array([-1, 5, -2, 4, 3, 1, 2, 8, 3])
    dl = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0])
    dh = np.array([5, 5, 5, 5, 5, 5, 5, 5, 5])
    Jb = SimplexMethodSolver(A, b, c).first_phase().basic_indexes

    solver = DualSimplexMethodSolver(A, b, c, dl, dh, Jb)
    res = solver.solve()

    print(res.max)