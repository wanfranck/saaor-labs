//
// Created by wanfranck on 28.09.16.
//

#include <bits/stdc++.h>

using namespace std;

#define ul unsigned long
#define ll long long
#define ld long double
#define mp make_pair
#define pb push_back
#define ppb pop_back
#define fst first
#define snd second

const int MAXN = (int)1e6 + 6;
const int INF = (int)1e9 + 7;

int n, m;

int main(){
    freopen("floyd.in", "r", stdin);
    freopen("floyd.out", "w", stdout);

    cin >> n >> m;
    vector<vector<int> > g((ul)n);
    for (int i = 0; i < n; i++){
        vector<int> t((ul)n, INF);
        g[i] = t;
        g[i][i] = 0;
    }

    for (int i = 0; i < m; i++){
        int in, out, dist;
        cin >> out >> in >> dist;
        g[out][in] = min(g[out][in], dist);
    }

    for (int i = 0; i < n; i++){
        for (int j = 0; j < n; j++){
            for (int k = 0; k < n; k++){
                g[i][j] = min(g[i][j], g[i][k] + g[k][j]);
            }
        }
    }

    for (int i = 0; i < n; i++){
        for (int j = 0; j < n; j++){
            cout << g[i][j] << ' ';
        }
        cout << endl;
    }

    return 0;
}