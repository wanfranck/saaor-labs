from dual_simplex_method import DualSimplexMethodSolver
from simplex_method import SimplexMethodSolver
import numpy as np


class GomoriSolver(object):
    def __init__(self, A, b, c, dl, dh):
        self.A = A
        self.m, self.n = A.shape
        self.b = b
        self.c = c
        self.dl = dl
        self.dh = dh
        self.x = None
        self.max = None

    def solve(self):
        eps = 1e-5
        start_n = self.n

        used = {}

        step = 0

        Jb = SimplexMethodSolver(self.A[:], self.b[:], self.c[:]).first_phase().basic_indexes

        while True:
            step += 1
            res = DualSimplexMethodSolver(self.A[:], self.b[:], self.c[:], self.dl[:], self.dh[:], Jb[:]).solve()

            inds_to_del = []
            if any([ind >= start_n for ind in res.Jb]):
                for i, ind in enumerate(res.Jb):
                    if ind >= start_n:
                        inds_to_del.append(ind)

            for ind in inds_to_del:
                res.Jb.pop(res.Jb.index(ind))

            def remove_synth(ind):
                val_ind = ind
                str_ind = val_ind - self.n + self.m

                a = self.A[str_ind][:]
                b = self.b[str_ind]

                self.A = np.delete(self.A, str_ind, 0)
                self.b = np.delete(self.b, str_ind, 0)
                self.c = np.delete(self.c, val_ind, 0)
                self.dl = np.delete(self.dl, val_ind, 0)
                self.dh = np.delete(self.dh, val_ind, 0)

                a = [aj / a[val_ind] if j != val_ind else aj for j, aj in enumerate(a)]
                b = b / a[val_ind]
                a.pop(val_ind)

                new_A = []
                for ind, str in enumerate(self.A):
                    coef = str[val_ind]
                    str = np.delete(str, val_ind, 0)
                    str -= [coef * aj for aj in a[:]]
                    self.b[ind] -= coef * b
                    new_A.append(str)

                self.A = np.array(new_A)
                self.m, self.n = self.A.shape

            inds_to_del = reversed(sorted(inds_to_del))

            for ind in inds_to_del:
                remove_synth(ind)

            for ind in inds_to_del:
                for j in res.Jb:
                    if j >= ind:
                        j -= 1

            self.m, self.n = self.A.shape
            Ab = self.A[:, res.Jb]
            B = np.linalg.inv(Ab)

            def is_integer(val):
                return abs(val - (val + eps) // 1) <= eps

            is_all_integers = all([is_integer(val) for val in res.hi])

            if is_all_integers:
                self.x, self.max = res.hi[range(start_n)], res.max
                break

            jk, k = 1e32, -1
            if tuple(res.hi) not in used.keys():
                used[tuple(res.hi)] = []

            for ind, val in enumerate(res.Jb):
                if not is_integer(res.hi[val]) and val not in used[tuple(res.hi)]:
                    jk, k = val, ind
                    used[tuple(res.hi)].append(val)
                    break

            if k == -1:
                break

            y = np.dot(np.eye(len(res.Jb), len(res.Jb))[:, k], B)
            betta = np.dot(y, self.b)
            a = np.dot(y, self.A)

            def integer(val):
                if val >= 0:
                    return int(val)
                else:
                    return int(val - 1.0)

            new_A = []
            for ind in range(self.m):
                new_A.append(np.append(self.A[ind], 0))

            new_cond = np.append([-(aj - (aj + eps) // 1) for aj in a], 1.0)
            new_A.append(new_cond)

            self.A = np.array(new_A)
            self.b = np.append(self.b, -(betta - (betta + eps) // 1))
            self.c = np.append(self.c, 0)
            Jb = res.Jb[:]
            Jb.append(len(self.c) - 1)
            self.dl = np.append(self.dl, 0)
            self.dh = np.append(self.dh, 1e32)
            self.m, self.n = self.A.shape

        return self

if __name__ == '__main__':
    print('Task 1. Answer: 160.')
    A = np.array([
        [1, -5, 3, 1, 0, 0],
        [4, -1, 1, 0, 1, 0],
        [2, 4, 2, 0, 0, 1]
    ])

    b = np.array([-8, 22, 30])
    c = np.array([7, -2, 6, 0, 5, 2])

    dl = np.array([0, 0, 0, 0, 0, 0])
    inf = 1e32
    dh = np.array([inf, inf, inf, inf, inf, inf])

    solver = GomoriSolver(A, b, c, dl, dh)
    res = solver.solve()

    print(res.max)

    print('Task 3. Answer: 311')
    A = np.array([
        [1, 0, 0, 12, 1, -3, 4, -1],
        [0, 1, 0, 11, 12, 3, 5, 3],
        [0, 0, 1, 1, 0, 22, -2, 1]
    ])

    b = np.array([40, 107, 61])
    c = np.array([2, 1, -2, -1, 4, -5, 5, 5])

    dl = np.array([0, 0, 0, 0, 0, 0, 0, 0])
    inf = 1e32
    dh = np.array([inf, inf, inf, inf, inf, inf, inf, inf])

    solver = GomoriSolver(A, b, c, dl, dh)
    res = solver.solve()

    print(res.max)

    print('Task 5. Answer: 21')
    A = np.array([
        [2, 1, -1, -3, 4, 7],
        [0, 1, 1, 1, 2, 4],
        [6, -3, -2, 1, 1, 1]
    ])

    b = np.array([7, 16, 6])
    c = np.array([1, 2, 1, -1, 2, 3])

    dl = np.array([0, 0, 0, 0, 0, 0])
    inf = 1e32
    dh = np.array([inf, inf, inf, inf, inf, inf])

    solver = GomoriSolver(A, b, c, dl, dh)
    res = solver.solve()

    print(res.max)

    print('Task 6. Answer: 157')
    A = np.array([
        [0, 7, 1, -1, -4, 2, 4],
        [5, 1, 4, 3, -5, 2, 1],
        [2, 0, 3, 1, 0, 1, 5]
    ])

    b = np.array([12, 27, 19])
    c = np.array([10, 2, 1, 7, 6, 3, 1])

    dl = np.array([0, 0, 0, 0, 0, 0, 0])
    inf = 1e32
    dh = np.array([inf, inf, inf, inf, inf, inf, inf])

    solver = GomoriSolver(A, b, c, dl, dh)
    res = solver.solve()

    print(res.max)

    print('Task 7. Answer: 26')
    A = np.array([
        [0, 7, -8, -1, 5, 2, 1],
        [3, 2, 1, -3, -1, 1, 0],
        [1, 5, 3, -1, -2, 1, 0],
        [1, 1, 1, 1, 1, 1, 1]
    ])

    b = np.array([6, 3, 7, 7])
    c = np.array([2, 9, 3, 5, 1, 2, 4])

    dl = np.array([0, 0, 0, 0, 0, 0, 0])
    inf = 1e32
    dh = np.array([inf, inf, inf, inf, inf, inf, inf])

    solver = GomoriSolver(A, b, c, dl, dh)
    res = solver.solve()

    print(res.max)

    print('Task 9. Answer: 25')
    A = np.array([
        [1, -3, 2, 0, 1, -1, 4, -1, 0],
        [1, -1, 6, 1, 0, -2, 2, 2, 0],
        [2, 2, -1, 1, 0, -3, 2, -1, 1],
        [4, 1, 0, 0, 1, -1, 0, -1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1, 1]
    ])

    b = np.array([3, 9, 9, 5, 9])
    c = np.array([-1, 5, -2, 4, 3, 1, 2, 8, 3])

    dl = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0])
    inf = 1e32
    dh = np.array([inf, inf, inf, inf, inf, inf, inf, inf, inf])

    solver = GomoriSolver(A, b, c, dl, dh)
    res = solver.solve()

    print(res.max)

    # Cycled tests because of accuracy
    '''
    print('Task 2. Answer: 23')
    A = np.array([
        [1, -3, 2, 0, 1, -1, 4, -1, 0],
        [1, -1, 6, 1, 0, -2, 2, 2, 0],
        [2, 2, -1, 1, 0, -3, 8, -1, 1],
        [4, 1, 0, 0, 1, -1, 0, -1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1, 1]
    ])

    b = np.array([3, 9, 9, 5, 9])
    c = np.array([-1, 5, -2, 4, 3, 1, 2, 8, 3])

    dl = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0])
    inf = 1e32
    dh = np.array([inf, inf, inf, inf, inf, inf, inf, inf, inf])

    solver = GomoriSolver(A, b, c, dl, dh)
    res = solver.solve()

    print(res.max)

    print('Task 4. Answer: 543')
    A = np.array([
        [1, 2, 3, 12, 1, -3, 4, -1, 2, 3],
        [0, 2, 0, 11, 12, 3, 5, 3, 4, 5],
        [0, 0, 2, 1, 0, 22, -2, 1, 6, 7]
    ])

    b = np.array([153, 123, 112])
    c = np.array([2, 1, -2, -1, 4, -5, 5, 5, 1, 2])

    dl = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
    inf = 1e32
    dh = np.array([inf, inf, inf, inf, inf, inf, inf, inf, inf, inf])

    solver = GomoriSolver(A, b, c, dl, dh)
    res = solver.solve()

    print(res.max)

    print('Task 8. Answer: -16')
    A = np.array([
        [1, 0, -1, 3, -2, 0, 1],
        [0, 2, 1, -1, 0, 3, -1],
        [1, 2, 1, 4, 2, 1, 1]
    ])

    b = np.array([4, 8, 24])
    c = np.array([-1, -3, -7, 0, -4, 0, -1])

    dl = np.array([0, 0, 0, 0, 0, 0, 0])
    inf = 1e32
    dh = np.array([inf, inf, inf, inf, inf, inf, inf])

    solver = GomoriSolver(A, b, c, dl, dh)
    res = solver.solve()

    print(res.max)
    '''



