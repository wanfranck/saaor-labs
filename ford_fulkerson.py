

class FordFulkersonSolver(object):
    def __init__(self, c):
        self.c = c
        self.n = len(c)
        self.f = [[[c[i][j][0], 0] for j in range(len(c[i]))] for i in range(self.n)]
        self.s = 0
        self.t = len(c) - 1

    def find_path(self):
        s, t, n = self.s, self.t, self.n
        q, flow, link, ind = [], [0] * n, [0] * n, [-1] * n
        flow[s], link[t] = 1e16, -1
        q.append(s)
        I = []

        while link[t] == -1 and len(q) > 0:
            u = q[0]
            I.append(u)
            q.pop(0)

            for i, edge in enumerate(self.c[u]):
                v, c = edge
                if c > self.f[u][i][1] and flow[v] == 0:
                    q.append(v)
                    link[v], ind[v] = u, i
                    if c - self.f[u][i][1] < flow[u]:
                        flow[v] = c - self.f[u][i][1]
                    else:
                        flow[v] = flow[u]

        if link[t] == -1:
            return [flow[t], I]

        q, u = [], t
        while u != s:
            self.f[link[u]][ind[u]][1] += flow[t]
            u = link[u]

        return [flow[t], I]

    def max_flow(self):
        ans, first, addPath = 0, True, [0, []]
        last = []
        while first or addPath[0] > 0:
            first = False
            addPath = self.find_path()
            ans += addPath[0]
            last = addPath[1]

        self.ans, self.U = ans, last

        return self


if __name__ == '__main__':
    c = [
        [[1, 6], [2, 6]],
        [[3, 4], [4, 2]],
        [[1, 5], [4, 9]],
        [[5, 4], [6, 2]],
        [[3, 8], [6, 7]],
        [[7, 7]],
        [[5, 11], [7, 4]],
        []
    ]

    solver = FordFulkersonSolver(c)
    print(solver.max_flow().U)