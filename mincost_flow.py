

class MinCostFlowSolver(object):
    def __init__(self, c, x, n, Ub):
        self.x = x
        self.c = c
        self.n = n
        self.Ub = Ub

    def count_potentials(self):
        u = [0] * self.n
        used = [False] * self.n

        call_queue = [0]

        while len(call_queue) > 0:
            this = call_queue[0]
            used[this] = True
            call_queue.pop(0)

            from_this, to_this = [], []

            for edge in self.Ub:
                if edge[0] == this:
                    if not used[edge[1]]:
                        from_this.append([edge[1], self.c[tuple(edge)]])
                        call_queue.append(edge[1])
                if edge[1] == this:
                    if not used[edge[0]]:
                        to_this.append([edge[0], self.c[tuple(edge)]])
                        call_queue.append(edge[0])

            for ind, c in from_this:
                if not used[ind]:
                    u[ind] = u[this] - c

            for ind, c in to_this:
                if not used[ind]:
                    u[ind] = u[this] + c

        return u

    def count_delta(self, u):
        Un = [item for item in self.c.keys() if item not in self.Ub]
        delta = {}

        for edge in Un:
            delta[tuple(edge)] = u[edge[0]] - u[edge[1]] - self.c[tuple(edge)]

        return delta

    def get_cycle(self):
        g = [[i] for i in range(self.n)]

        for edge in self.Ub:
            fr, to = edge
            g[fr].append(to)
            g[to].append(fr)

        for i in range(self.n):
            g[i].pop(0)

        vertex_stack, cycle, Uc = [], [], []
        used = [False] * self.n

        ans = None

        for i in range(self.n):
            if not used[i]:
                vertex_stack.append(i)
                while ans is None or len(vertex_stack) > 0:
                    ind = vertex_stack[len(vertex_stack) - 1]
                    used[ind] = True
                    if len(vertex_stack) > 1:
                        prev = vertex_stack[len(vertex_stack) - 2]
                    else:
                        prev = -1

                    if len(g[ind]) == 0:
                        vertex_stack.pop(len(vertex_stack) - 1)

                    chosen = False
                    for i, v in enumerate(g[ind]):
                        if ans is not None:
                            return
                        if not used[v]:
                            vertex_stack.append(v)
                            g[ind].pop(i)
                            chosen = True
                            break
                        elif v != prev:
                            vertex_stack.append(v)
                            cycle = vertex_stack[vertex_stack.index(v): len(vertex_stack)]
                            for ind in range(1, len(cycle)):
                                if tuple([cycle[ind], cycle[ind - 1]]) in self.Ub:
                                    Uc.append(tuple([cycle[ind], cycle[ind - 1]]))
                                if tuple([cycle[ind - 1], cycle[ind]]) in self.Ub:
                                    Uc.append(tuple([cycle[ind - 1], cycle[ind]]))

                            return Uc

                    if chosen:
                        continue

                    vertex_stack.pop(len(vertex_stack) - 1)
        return ans

    def get_rotation_sets(self, Uc, edge0):
        Uc_pos, Uc_neg = [], []
        Uc_pos.append(edge0)
        dir = 1
        last = edge0[1]

        Uc_tmp = Uc[:]
        Uc_tmp.pop(Uc_tmp.index(edge0))

        while len(Uc_tmp) > 0:
            new = -1

            if dir == 1:
                for i, edge in enumerate(Uc_tmp):
                    if edge[0] == last:
                        new = edge[1]
                        Uc_pos.append(edge)
                        Uc_tmp.pop(i)
                        break

                if new == -1:
                    dir = -1
                    for i, edge in enumerate(Uc_tmp):
                        if edge[1] == last:
                            new = edge[0]
                            Uc_neg.append(edge)
                            Uc_tmp.pop(i)
                            break
            else:
                for i, edge in enumerate(Uc_tmp):
                    if edge[1] == last:
                        new = edge[0]
                        Uc_neg.append(edge)
                        Uc_tmp.pop(i)
                        break

                if new == -1:
                    dir = 1
                    for i, edge in enumerate(Uc_tmp):
                        if edge[0] == last:
                            new = edge[1]
                            Uc_pos.append(edge)
                            Uc_tmp.pop(i)
                            break

            last = new

        return [Uc_pos, Uc_neg]

    def solve(self):
        while True:
            u = self.count_potentials()
            delta = self.count_delta(u)

            edge0, val0 = [-1, -1], 0
            for key, val in delta.items():
                if val > 0:
                    edge0, val0 = tuple([key[0], key[1]]), val

            if edge0 == [-1, -1]:
                return self

            self.Ub.append(tuple(edge0))

            Uc = self.get_cycle()

            Uc_pos, Uc_neg = self.get_rotation_sets(Uc, edge0)

            edge_ast, tetta = [-1, -1], 1e32
            for edge in Uc_neg:
                if tetta > self.x[edge]:
                    tetta = self.x[edge]
                    edge_ast = edge

            index = self.Ub.index(edge_ast)
            self.Ub.pop(index)

if __name__ == '__main__':
    n = 6
    Ub = [tuple([0, 1]), tuple([2, 1]), tuple([5, 2]), tuple([2, 3]), tuple([4, 3])]
    c = {
        tuple([0, 1]): 1, tuple([5, 0]): -2, tuple([1, 5]): 3, tuple([2, 1]): 3, tuple([5, 2]): 3,
        tuple([5, 4]): 4, tuple([4, 2]): 4, tuple([2, 3]): 5, tuple([4, 3]): 1
    }
    x = {
        tuple([0, 1]): 1, tuple([5, 0]): 0, tuple([1, 5]): 0, tuple([2, 1]): 3, tuple([5, 2]): 9,
        tuple([5, 4]): 0, tuple([4, 2]): 0, tuple([2, 3]): 1, tuple([4, 3]): 5
    }

    solver = MinCostFlowSolver(c, x, n, Ub)
    res = solver.solve()

    print(res.Ub)