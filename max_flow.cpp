//
// Created by wanfranck on 28.09.16.
//

#include <bits/stdc++.h>

using namespace std;

#define ul unsigned long
#define ll long long
#define ld long double
#define mp make_pair
#define pb push_back
#define ppb pop_back
#define fst first
#define snd second

const int MAXN = (int)1e6 + 6;
const int INF = (int)1e9 + 7;

int n, m, s, t;
vector<vector<pair<int, int> > > c, f;

int findPath(int s, int t){
    queue<int> q;
    vector<int> flow((ul)n, 0), link((ul)n, 0);
    vector<int> ind((ul)n, -1);

    q.push(s);

    link[t] = -1, flow[s] = INF;

    while (link[t] == -1 && !q.empty()){
        int u = q.front();
        q.pop();
        for (int i = 0; i < f[u].size(); i++){
            int v = f[u][i].fst;
            if (c[u][i].snd > f[u][i].snd && flow[v] == 0){
                q.push(v), link[v] = u, ind[v] = i;
                if (c[u][i].snd - f[u][i].snd < flow[u]){
                    flow[v] = c[u][i].snd - f[u][i].snd;
                }
                else{
                    flow[v] = flow[u];
                }
            }
        }
    }

    if (link[t] == -1) return 0;
    while (!q.empty()) q.pop();

    int u = t;
    while (u != s){
        f[link[u]][ind[u]].snd += flow[t];
        u = link[u];
    }

    return flow[t];
}

int maxFlow(int s, int t){
    int ans = 0, addFlow;
    do{
        addFlow = findPath(s, t);
        ans += addFlow;
    } while(addFlow > 0);

    return ans;
}

int main(){
    freopen("max_flow.in", "r", stdin);
    freopen("max_flow.out", "w", stdout);

    cin >> n >> m;
    c.resize((ul)n, vector<pair<int, int> >(0)), f.resize((ul)n, vector<pair<int, int> >(0));

    s = 0, t = n - 1;
    for (int i = 0; i < m; i++){
        int in, out, cost;
        cin >> out >> in >> cost;
        c[out].pb(mp(in, cost));
        f[out].pb(mp(in, 0));
    }

    cout << maxFlow(s, t) << endl;

    return 0;
}

