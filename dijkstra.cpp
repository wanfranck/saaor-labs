//
// Created by wanfranck on 28.09.16.
//

#include <bits/stdc++.h>

using namespace std;

#define ul unsigned long
#define ll long long
#define ld long double
#define mp make_pair
#define pb push_back
#define ppb pop_back
#define fst first
#define snd second

const int MAXN = (int)1e6 + 6;
const int INF = (int)1e9 + 7;

int n, m;

int main(){
    freopen("dijkstra.in", "r", stdin);
    freopen("dijkstra.out", "w", stdout);

    cin >> n >> m;
    vector<vector<pair<int, int> > > g((ul)n);
    for (int i = 0; i < m; i++){
        int in, out, dist;
        cin >> out >> in >> dist;
        g[out].pb(mp(dist, in));
    }

    int s;
    cin >> s;

    vector<int> d((ul)n, INF);
    d[s] = 0;

    queue<pair<int, int> > q;
    q.push(mp(0, s));

    while (!q.empty()){
        int cur = q.front().snd, dist = - q.front().fst;
        q.pop();

        if (dist > d[cur])
            continue;

        for (int i = 0; (unsigned int)i < g[cur].size(); i++){
            int to = g[cur][i].snd, distTo = g[cur][i].fst;

            if (dist + distTo < d[to]){
                d[to] = dist + distTo;
                q.push(mp(- (dist + distTo), to));
            }
        }
    }

    for (int i = 0; (unsigned int)i < d.size(); i++){
        cout << d[i] << ' ';
    }

    return 0;
}
