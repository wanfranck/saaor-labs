from dual_simplex_method import DualSimplexMethodSolver
from simplex_method import SimplexMethodSolver
import numpy as np


class Task(object):
    def __init__(self, A, b, c, dl, dh):
        self.A = A
        self.b = b
        self.c = c
        self.dl = dl
        self.dh = dh


class BranchNBoundSolver(object):
    def __init__(self, A, b, c, dl, dh, r=-np.inf):
        self.A = A
        self.b = b
        self.c = c
        self.dl = dl
        self.dh = dh
        self.r = r
        self.tasks = [Task(self.A, self.b, self.c, self.dl, self.dh)]
        self.mu = None
        self.mu_0 = 0

    def solve(self):
        eps = 1e-9
        while len(self.tasks) > 0:
            task = self.tasks[0]
            self.tasks.pop(0)

            Jb = SimplexMethodSolver(task.A, task.b, task.c).solve().basic_indexes
            dual_simplex_solver = DualSimplexMethodSolver(task.A, task.b, task.c, task.dl, task.dh, Jb)
            res = dual_simplex_solver.solve()

            if res.hi is None:
                continue

            if res.max <= self.r:
                continue

            def is_integer(val):
                return abs(val - (val + eps) // 1) <= eps

            is_all_integer = all([is_integer(val) for val in res.hi])

            if is_all_integer:
                self.r, self.mu, self.mu_0 = res.max, res.hi, 1
                continue

            x_j0, j0 = res.hi[0], 0
            for ind, val in enumerate(res.hi):
                if not is_integer(val):
                    x_j0, j0 = val, ind
                    break

            def integer(val):
                if val >= 0:
                    return int(val)
                else:
                    return int(val - 1.0)

            l_j0 = integer(x_j0)

            new_dh = np.array(task.dh)
            new_dh[j0] = l_j0

            new_dl = np.array(task.dl)
            new_dl[j0] = l_j0 + 1

            self.tasks.append(Task(task.A, task.b, task.c, task.dl, new_dh))
            self.tasks.append(Task(task.A, task.b, task.c, new_dl, task.dh))

        return self

if __name__ == '__main__':
    print('Test 1. Answer: 39')
    A = np.array([
        [1, 0, 0, 12, 1, -3, 4, -1],
        [0, 1, 0, 11, 12, 3, 5, 3],
        [0, 0, 1, 1, 0, 22, -2, 1]
    ])

    b = np.array([40, 107, 61])
    c = np.array([2, 1, -2, -1, 4, -5, 5, 5])
    dl = np.array([0, 0, 0, 0, 0, 0, 0, 0])
    dh = np.array([3, 5, 5, 3, 4, 5, 6, 3])

    solver = BranchNBoundSolver(A, b, c, dl, dh)
    res = solver.solve()

    print(res.r)

    print('Test 2. Answer: 23')
    A = np.array([
        [1, -3, 2, 0, 1, -1, 4, -1, 0],
        [1, -1, 6, 1, 0, -2, 2, 2, 0],
        [2, 2, -1, 1, 0, -3, 8, -1, 1],
        [4, 1, 0, 0, 1, -1, 0, -1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1, 1]
    ])

    b = np.array([3, 9, 9, 5, 9])
    c = np.array([-1, 5, -2, 4, 3, 1, 2, 8, 3])
    dl = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0])
    dh = np.array([5, 5, 5, 5, 5, 5, 5, 5, 5])

    solver = BranchNBoundSolver(A, b, c, dl, dh)
    res = solver.solve()

    print(res.r)

    print('Test 3. Answer: 29')
    A = np.array([
        [1, 0, 0, 12, 1, -3, 4, -1, 2.5, 3],
        [0, 1, 0, 11, 12, 3, 5, 3, 4, 5.1],
        [0, 0, 1, 1, 0, 22, -2, 1, 6.1, 7]
    ])

    b = np.array([43.5, 107.3, 106.3])
    c = np.array([2, 1, -2, -1, 4, -5, 5, 5, 1, 2])
    dl = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
    dh = np.array([2, 4, 5, 3, 4, 5, 4, 4, 5, 6])

    solver = BranchNBoundSolver(A, b, c, dl, dh)
    res = solver.solve()

    print(res.r)

    print('Test 4. Answer: 26')
    A = np.array([
        [4, 0, 0, 0, 0, -3, 4, -1, 2, 3],
        [0, 1, 0, 0, 0, 3, 5, 3, 4, 5],
        [0, 0, 1, 0, 0, 22, -2, 1, 6, 7],
        [0, 0, 0, 1, 0, 6, -2, 7, 5, 6],
        [0, 0, 0, 0, 1, 5, 5, 1, 6, 7]
    ])

    b = np.array([8, 5, 4, 7, 8])
    c = np.array([2, 1, -2, -1, 4, -5, 5, 5, 1, 2])
    dl = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
    dh = np.array([10, 10, 10, 10, 10, 10, 10, 10, 10, 10])

    solver = BranchNBoundSolver(A, b, c, dl, dh)
    res = solver.solve()

    print(res.r)

    print('Test 5. Answer: 53')
    A = np.array([
        [1, -5, 3, 1, 0, 0],
        [4, -1, 1, 0, 1, 0],
        [2, 4, 2, 0, 0, 1]
    ])

    b = np.array([-8, 22, 30])
    c = np.array([7, -2, 6, 0, 5, 2])
    dl = np.array([2, 1, 0, 0, 1, 1])
    dh = np.array([6, 6, 5, 2, 4, 6])

    solver = BranchNBoundSolver(A, b, c, dl, dh)
    res = solver.solve()

    print(res.r)

    print('Test 6. Answer: 70')
    A = np.array([
        [1, 0, 0, 3, 1, -3, 4, -1],
        [0, 1, 0, 4, -3, 3, 5, 3],
        [0, 0, 1, 1, 0, 2, -2, 1]
    ])

    b = np.array([30, 78, 5])
    c = np.array([2, 1, -2, -1, 4, -5, 5, 5])
    dl = np.array([0, 0, 0, 0, 0, 0, 0, 0])
    dh = np.array([5, 5, 3, 4, 5, 6, 6, 8])

    solver = BranchNBoundSolver(A, b, c, dl, dh)
    res = solver.solve()

    print(res.r)

    print('Test 7. Answer: 78')
    A = np.array([
        [1, -3, 2, 0, 1, -1, 4, -1, 0],
        [1, -1, 6, 1, 0, -2, 2, 2, 0],
        [2, 2, -1, 1, 0, -3, 2, -1, 1],
        [4, 1, 0, 0, 1, -1, 0, -1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1, 1]
    ])

    b = np.array([18, 18, 30, 15, 18])
    c = np.array([7, 5, -2, 4, 3, 1, 2, 8, 3])
    dl = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0])
    dh = np.array([8, 8, 8, 8, 8, 8, 8, 8, 8])

    solver = BranchNBoundSolver(A, b, c, dl, dh)
    res = solver.solve()

    print(res.r)

    print('Test 8. Answer: 18')
    A = np.array([
        [1, 0, 1, 0, 4, 3, 4],
        [0, 1, 2, 0, 55, 3.5, 5],
        [0, 0, 3, 1, 6, 2, -2.5]
    ])

    b = np.array([26, 185, 32.5])
    c = np.array([1, 2, 3, -1, 4, -5, 6])
    dl = np.array([0, 1, 0, 0, 0, 0, 0])
    dh = np.array([1, 2, 5, 7, 8, 4, 2])

    solver = BranchNBoundSolver(A, b, c, dl, dh)
    res = solver.solve()

    print(res.r)

    print('Test 9. Answer: 74')
    A = np.array([
        [2, 0, 1, 0, 0, 3, 5],
        [0, 2, 2.1, 0, 0, 3.5, 5],
        [0, 0, 3, 2, 0, 2, 1.1],
        [0, 0, 3, 0, 2, 2, -2.5]
    ])

    b = np.array([58, 66.3, 36.7, 13.5])
    c = np.array([1, 2, 3, 1, 2, 3, 4])
    dl = np.array([1, 1, 1, 1, 1, 1, 1])
    dh = np.array([2, 3, 4, 5, 8, 7, 7])

    solver = BranchNBoundSolver(A, b, c, dl, dh)
    res = solver.solve()

    print(res.r)

    print('Test 10. Answer: 40')
    A = np.array([
        [1, 0, 0, 1, 1, -3, 4, -1, 3, 3],
        [0, 1, 0, -2, 1, 1, 7, 3, 4, 5],
        [0, 0, 1, 1, 0, 2, -2, 1, -4, 7]
    ])

    b = np.array([27, 6, 18])
    c = np.array([-2, 1, -2, -1, 8, -5, 3, 5, 1, 2])
    dl = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
    dh = np.array([8, 7, 6, 7, 8, 5, 6, 7, 8, 5])

    solver = BranchNBoundSolver(A, b, c, dl, dh)
    res = solver.solve()

    print(res.r)
