from assignment_problem import AssignmentProblemSolver
import copy


class Task(object):
    def __init__(self, c):
        self.c = c


class DoorToDoorSolver(object):
    def __init__(self, c):
        self.c = [item[:] for item in c]
        self.n = len(c)
        self.x = []
        self.r = None
        self.B = []

    def get_cycles(self, I):
        cycles = []
        cycles.append([I[0]])
        I.pop(0)

        while len(I) > 0:
            was = False

            for i, item in enumerate(I):
                for cycle in cycles:
                    if item[1] == cycle[0][0]:
                        cycle.insert(0, item)
                        I.pop(i)
                        was = True
                        break
                    elif item[0] == cycle[len(cycle) - 1][1]:
                        cycle.append(item)
                        I.pop(i)
                        was = True
                        break

            if not was:
                cycles.append([I[0]])
                I.pop(0)

        return cycles

    def solve(self):
        INF = 1e9 + 7

        n = self.n
        self.x = [[1 if j == (i + 1) % n else 0 for j in range(n)] for i in range(n)]
        self.r = sum([self.x[i][j] * self.c[i][j] for j in range(n) for i in range(n) if self.x[i][j] == 1])

        task = Task([item for item in self.c])

        tasks = [task]

        while len(tasks) > 0:
            task = tasks[0].c
            tasks.pop(0)

            solver = AssignmentProblemSolver([item for item in task])
            res = solver.solve()

            if res.F >= self.r:
                continue

            cycles = self.get_cycles([item for item in res.B])

            if len(cycles) == 1:
                self.r, self.x, self.B = res.F, res.x, res.B
                continue

            min_i = 0
            for i, cycle in enumerate(cycles):
                if len(cycle) < len(cycles[min_i]):
                    min_i = i

            for edge in cycles[min_i]:
                new_c = [item[:] for item in task]
                new_c[edge[0]][edge[1]] = INF
                tasks.append(Task(new_c))

        return self


if __name__ == '__main__':
    INF = 1e9 + 7
    c = [
        [INF, 10, 25, 25, 10],
        [1, INF, 10, 15, 2],
        [8, 9, INF, 20, 10],
        [14, 10, 24, INF, 15],
        [10, 8, 25, 27, INF]
    ]

    solver = DoorToDoorSolver(c)
    res = solver.solve()

    print(res.B)
